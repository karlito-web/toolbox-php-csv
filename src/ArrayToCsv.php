<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Csv;

use League\Csv\CannotInsertRecord;
use League\Csv\CharsetConverter;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\UnavailableStream;
use League\Csv\Writer;
use SplFileInfo;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://csv.thephpleague.com/9.0/writer/
 * @package     karlito-web/toolbox-php-csv
 * @subpackage  league/csv
 * @version     3.1.0
 */
class ArrayToCsv
{
    /**
     * Transform an array to a CSV File
     *
     * @param string $filepath          The path to the CSV file
     * @param array $header             The first line
     * @param array $rows               All the lines
     * @param string $mode              The type access you require to the stream
     * @return void
     * @throws InvalidArgument
     * @throws UnavailableStream
     * @throws CannotInsertRecord
     */
    public static function generate(string $filepath, array $header, array $rows, string $mode = 'w'): void
    {
        // Create
        $csv = Writer::createFromPath($filepath, $mode);

        // Config
        $csv->setDelimiter(";");
        $csv->setEnclosure('"');
        $csv->setEndOfLine(PHP_EOL);
        $csv->setEscape('/');

        // Insert Header
        $obj = new SplFileInfo($csv->getPathname());
        if ($obj->getSize() === 0) {
            try {
                $csv->insertOne($header);
            } catch (CannotInsertRecord|Exception $e) {
                throw new CannotInsertRecord($e->getMessage());
            }
        }

        // Insert Datas
        try {
            // Encoder
            foreach ($rows as $row) {
                foreach ($row as $value) {
                    $encoder = self::getEncoder($value);
                }
            }
            $encoder->convert($rows);
            $csv->insertAll($rows);
        } catch (CannotInsertRecord|Exception $e) {
            throw new CannotInsertRecord($e->getMessage());
        }
    }

    /**
     * @param mixed $text
     * @return CharsetConverter
     */
    private static function getEncoder(mixed $text): CharsetConverter
    {
        $encodages = mb_list_encodings();
        $encodage  = mb_detect_encoding((string) $text, implode(', ', $encodages));

        // Encoder
        if ($encodage !== 'UTF-8') {
            return (new CharsetConverter())->inputEncoding($encodage)->outputEncoding('iso-8859-15');
        } else {
            return (new CharsetConverter())->inputEncoding('UTF-8')->outputEncoding('iso-8859-15');
        }
    }
}

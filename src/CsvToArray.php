<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Csv;

use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\Reader;
use League\Csv\UnavailableStream;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://csv.thephpleague.com/9.0/reader/
 * @package     karlito-web/toolbox-php-csv
 * @subpackage  league/csv
 * @version     3.1.0
 */
class CsvToArray
{
    /**
     * @param string $filepath          The path to the CSV file to parsed
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @param bool $header              See the first line
     * @return array
     * @throws Exception
     * @throws InvalidArgument
     * @throws UnavailableStream
     */
    public static function generate(
        string $filepath,
        string $delimiter = "\n",
        string $enclosure = '"',
        string $escape = '/',
        bool   $header = true
    ): array {
        // Array
        $return = [];

        // Reader
        $csv = Reader::createFromPath($filepath);
        $csv->setDelimiter($delimiter);
        $csv->setEnclosure($enclosure);
        $csv->setEscape($escape);

        if ($header === false) {
            $csv->setHeaderOffset(0);
        }

        // Records
        $records = $csv->getRecords();

        foreach ($records as $result) {
            $return[] = $result;
        }

        return $return;
    }
}

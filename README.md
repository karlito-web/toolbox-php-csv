# Karlito Web

###### Description:  
Helpers for CSV.

Convert CSV File to Array or Array to CSV File.

***
###### Installation:

```cli
composer require karlito-web/toolbox-php-csv
```

***
###### Usage:  

To Save in a CSV File
```php
use KarlitoWeb\Toolbox\Csv\Csv;

Csv::ArrayToCSV($filepath, $header, $rows);
```

To Convert CSV File to an Array
```php
use KarlitoWeb\Toolbox\Csv\Csv;

Csv::CSVToArray($filepath, $delimiter = "\n", $enclosure = '"', $escape = '/');
Csv::CSVToArrayWithLimit($filepath, $offset, $limit, $delimiter = "\n", $enclosure = '"', $escape = '/');
```

***
##### Contributing:  
Larger projects often have sections on contributing to their project, in which contribution instructions are outlined. Sometimes, this is a separate file. If you have specific contribution preferences, explain them so that other developers know how to best contribute to your work. To learn more about how to help others contribute, check out the guide for setting guidelines for repository contributors.

***
##### Credits:  
Include a section for credits in order to highlight and link to the authors of your project.

***
##### License:  
Finally, include a section for the license of your project. For more information on choosing a license, check out GitHub’s licensing guide!

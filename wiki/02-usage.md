# Usage
#### by Karlito Web

***

To convert CSV to PHP Array
``` php
use KarlitoWeb\Toolbox\Csv\CsvToArray;

$fn = new CsvToArray();

$array = $fn::generate(string $filepath, string $delimiter = "\n", string $enclosure = '"', string $escape = '/', bool $header = true, // ?int $offset = null, // ?int $limit = null): array;  
```

To save PHP Array in CSV File
``` php
use KarlitoWeb\Toolbox\Csv\ArrayToCsv;

$fn = new ArrayToCsv();

$string = $fn::generate(string $filepath, array $header, array $rows, string $mode = 'w');
```